<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'baashyam' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'X$^qwyF7lgaIhOF+$o>H_[oaK;iL@%]#,6R7R fz_^Sb>R<mR_`[#us*8xwrF(+8' );
define( 'SECURE_AUTH_KEY',  '8R0~z*}vVCJq}&zcgxvuZ1/k?EY[BSA7PMX~ OV3Vn*jPisfyl9g}G[loe:@ZT$f' );
define( 'LOGGED_IN_KEY',    'sXN$j9,c]e:4s3vk*N?V&(D&_7(K2yO6b!#cw[KLP^iS*fFp<9{>?!$-s0k38p[V' );
define( 'NONCE_KEY',        'kz`JBfi15:()R{13C0ufVv/6#K1^De9ojY}#A`*)%x(6@HUT}3xny~`^u4n3(!-@' );
define( 'AUTH_SALT',        '^,_}X?#^vZXE]/C^Q&i;v`y0G=Klll^u ?xv|)pzS&jTCNwCrm]x&|9z~S:8?LT~' );
define( 'SECURE_AUTH_SALT', '1bMvp}77_,e7.s,FC^]Ln;v^v` ewT|u<of!sKR*8b,0~f|QuAL,R28(#c7Tr;<@' );
define( 'LOGGED_IN_SALT',   ')i`]VK:r.=k@3o$i?gRy^1.VEM|&Sb*zD:#6._zYqA~=|VOEKZ}%Nl0,04/,gI|.' );
define( 'NONCE_SALT',       'm.An!UN -!hdS0l?REvjGG;nP~Mv>dMj{U1sDttnpZH+-]%CRDWSSn?_>``:x%?D' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'baas_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
