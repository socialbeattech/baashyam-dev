$(document).ready(function(){
//start of js for horizontal scrolling	
$get_width = $(window).width();
//alert($get_width);
if($get_width > 768 )
{
	$target  = $(".horizontal_all").first(),
	currentPosition = parseInt($target.css("transform").split(",")[4]),
	moveBy = function(scrolledBy){
	currentPosition += scrolledBy;
	$target.css("transform", "translateX(" + (currentPosition) + "px)")
	},
	lastScrollTop = 0 ;

	currentPosition = isNaN(currentPosition) ? 0 : currentPosition;
	$(window).bind("scroll",function(e){
	var scrolledBy = $(window).scrollTop() - lastScrollTop;
	moveBy(-scrolledBy);
	lastScrollTop = $(window).scrollTop();
	}); 	
}

//start of js for Counters at the footer
var top_counter = $('#counter').offset().top;
var top_counterlux = $('#counterlux').offset().top;
var top_counterhappy = $('#counterhappy').offset().top;
	$(window).scroll(function(){
		if($(window).scrollTop() > top_counter)
		{ 
			$({countNum: $('#counter').text()}).animate({countNum: 100  }, {
			duration: 5000,
			easing:'linear',
			step: function() {	$('#counter').text(Math.floor(this.countNum));	},
			complete: function() {	$('#counter').text(this.countNum);	}
			});
		}
		
		if($(window).scrollTop() > top_counterlux)
		{ 
			$({countNum: $('#counterlux').text()}).animate({countNum: 1600000  }, {
			duration: 5000,
			easing:'linear',
			step: function() {	$('#counterlux').text(Math.floor(this.countNum));	},
			complete: function() {	$('#counterlux').text(this.countNum);	}
			});
		}
				
		if($(window).scrollTop() > top_counterhappy)
		{ 
			$({countNum: $('#counterhappy').text()}).animate({countNum: 1600  }, {
			duration: 5000,
			easing:'linear',
			step: function() {	$('#counterhappy').text(Math.floor(this.countNum));	},
			complete: function() {	$('#counterhappy').text(this.countNum);	}
			});
		}
		
	});

});